const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const apiRoutes = require('./routes/ticket')
const app = express()
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/api', apiRoutes)
mongoose.connect(
    "mongodb://localhost:27017/busticketbooking", { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true }
)
mongoose.connection
    .once('open', () => console.log('Connected to the DB successfully'))
    .on('error', (error) => {
        console.warn('Warning', error);
    });

app.listen(3000, function() {
    console.log("Server started on port 3000");
});