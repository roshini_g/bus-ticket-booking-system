const express = require('express')
const Ticket = require('../models/Ticket')
const User = require('../models/User')
const router = express.Router()
router.post('/ticket', (req, res) => {
    const ticket = new Ticket({ seat_number: req.body.seat_number });
    const user = new User(req.body.passenger);
    user.save()
        .then(data => {
            if (data) {
                ticket.passenger = user._id
                ticket.save()
                    .then(data => res.status(200).json(data))
                    .catch(err => {
                        User.findOneAndDelete({ _id: user._id })
                            .then((data) => res.status(400))
                            .catch(err => res.status(400).json({ message: err }))
                    })
            }
        })
        .catch(err => res.status(404).json({ message: err }))

})
router.get('/ticket/:ticket_id', (req, res) => {
    const { ticket_id } = req.params
    Ticket.findById(ticket_id, function(err, ticket) {
        if (err) res.status(404).json({ message: err })
        if (ticket) res.status(200).json({ status: ticket.is_booked })
    })
})
router.get('/tickets/open', (req, res) => {
    Ticket.find({ is_booked: false }, (err, data) => {
        if (err) res.status(404).json({ message: err })
        if (data) res.status(200).json(data)
    })
})
router.get('/tickets/closed', (req, res) => {
    Ticket.find({ is_booked: true }, (err, data) => {
        if (err) res.status(404).json({ message: err })
        if (data) res.status(200).json(data)
    })
})
router.get('/ticket/details/:ticket_id', (req, res) => {
    const { ticket_id } = req.params
    Ticket.findById(ticket_id, function(err, ticket) {
        if (err) res.status(404).json({ message: err })
        if (ticket) {
            User.findById(ticket.passenger, function(err, user) {
                if (err) res.status(404).json({ message: err })
                if (user) res.status(200).json(user)
            })
        }
    })
})

function openTicket(ticket) {
    ticket.is_booked = false
    ticket.save()
        .then(data => console.log(`Opened ticket: ${ticket._id}`))
        .catch(err => console.log(`Failed to open ticket with ticketID: ${ticket._id}`))
}
router.post('/tickets/reset', (req, res) => {
    if (!("username" in req.body) && !("password" in req.body)) {
        res.status(400).json({ message: "username and password is needed in request body" })
    }
    const { username, password } = req.body
    const Username = "admin";
    const Password = 123456;
    if (!(username === Username)) {
        res.status(400).json({ message: "Incorrect username" })
    }
    if (!(password === Password)) {
        res.status(400).json({ message: "Incorrect password" })
    }
    Ticket.find({}, (err, data) => {
        if (err) res.status(404).json({ message: err })
        if (data) {
            data.forEach(openTicket)
            res.status(200).json({ message: "success" })
        }
    })

})
module.exports = router